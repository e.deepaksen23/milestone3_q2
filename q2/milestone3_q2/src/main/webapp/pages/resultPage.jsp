<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SALARY SPLIT</title>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
table, th, td {
  border:5px solid Magenta;
  padding: 1px;
  height: 50px;
  vertical-align: center;
  text-align:center;
}
table.center {
  margin-left: auto; 
  margin-right: auto;
  width: 40%;
}
tr:hover {background-color: black;}
tr:nth-child(even) {background-color: Tomato;}
tr:nth-child(odd) {background-color: cyan;}
</style>
<body>
<table class="center">
  <tr><td>BASIC PAY</td><td>+${basic}</td></tr>
  <tr><td>HOUSE RENT ALLOWANCE</td><td>+${hra}</td></tr>
  <tr><td>DEARNESS ALLOWANCE</td><td>+${da}</td></tr>
  <tr><td>GROSS PAY</td><td>${gross}</td></tr>
  <tr><td>PROVIDENT FUND</td><td>-${pf}</td></tr>
  <tr><td>NET PAY</td><td>${net}</td></tr>
  <tr><td>ANNUAL PACKAGE</td><td>${annual_package}</td></tr>
  </table>
</body>
</html>