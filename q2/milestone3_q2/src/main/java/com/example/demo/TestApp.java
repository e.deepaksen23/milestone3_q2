package com.example.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestApp {
	@RequestMapping("/")
	public String display1() {return "/homePage";}
	@RequestMapping("/find")
	public String display2(HttpServletRequest req,HttpServletResponse res,Model model) {
		double basic=Float.parseFloat(req.getParameter("basic"));
		double hra=basic*0.1;
		double da=basic*0.1;
		double pf=basic*0.1;
		double gross=basic+hra+da;
		double net=gross-pf;
		double annual_package=gross*12;
		
		model.addAttribute("basic",basic);
		model.addAttribute("da",da);
		model.addAttribute("hra",hra);
		model.addAttribute("pf",pf);
		model.addAttribute("gross",gross);
		model.addAttribute("net",net);
		model.addAttribute("annual_package",annual_package);
		return "/resultPage";}
}
